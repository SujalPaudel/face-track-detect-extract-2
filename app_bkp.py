#!/usr/bin/env python
#
# Project: Video Streaming with Flask
# Author: Log0 <im [dot] ckieric [at] gmail [dot] com>
# Date: 2014/12/21
# Website: http://www.chioka.in/
# Description:
# Modified to support streaming out with webcams, and not just raw JPEGs.
# Most of the code credits to Miguel Grinberg, except that I made a small tweak. Thanks!
# Credits: http://blog.miguelgrinberg.com/post/video-streaming-with-flask
#
# Usage:
# 1. Install Python dependencies: cv2, flask. (wish that pip install works like a charm)
# 2. Run "python main.py".
# 3. Navigate the browser to the local webpage.
from flask import Flask, render_template, Response, request
from flask_cors import CORS, cross_origin
# import people_counter

import psycopg2 as p
import json

app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

conn = p.connect("dbname = 'peopleCount' user = 'sujal' password = 'hyperl00p' host = 'localhost'")
cur = conn.cursor()

@app.route('/all_results', methods=['POST'])
@cross_origin()
def all_results():

	req_arg = request.form['date_per_vid']

	# req_arg = '2019-07-24'

	cur.execute("SELECT * FROM labimcount WHERE date = %s;", (req_arg,))

	row_headers=[x[0] for x in cur.description]
	rv = cur.fetchall()
	json_data=[]
	for result in rv:
		# print(result)
		json_data.append(dict(zip(row_headers,result)))
	return json.dumps(json_data)
	

@app.route('/day_analytics', methods=['POST'])
@cross_origin()
def day_analytics():
	req_arg = request.form['date_per_day']

	# req_arg = '2019-07-24'

	cur.execute("SELECT * FROM peoplecountperday WHERE date = %s;", (req_arg,))

	row_headers=[x[0] for x in cur.description]
	rv = cur.fetchall()
	json_data=[]

	return json.dumps(dict(zip(row_headers,rv)))



@app.route('/')
def index():
	return render_template('index.html')

@app.route('/dayAnalytics')
def dayAnalytics():
	return render_template('day_analytics.html')


# @app.route('/inputvalue')
# def index(inputvalue):
#   return ('hey' + inputvalue)
    

if __name__ == '__main__':
    app.run( host="0.0.0.0", port = 80)



